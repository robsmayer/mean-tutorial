"use strict";

module.exports = function (app, dirname) {

  app.get('/', function (req, res) {
    res.sendFile(dirname + '/client/views/index.html');
  });

  app.get('/exemplo', function (req, res) {
    res.sendFile(dirname + '/client/views/exemplo.html');
  });

  app.get('/about', function (req, res) {
    res.sendFile(dirname + '/client/views/about.html');
  });

  app.get('/contato', function (req,res) {
    res.sendFile(dirname + '/client/views/contato.html')
  });

  app.get('/team', function (req,res) {
    res.sendFile(dirname + '/client/views/Team.html')
  });


};
